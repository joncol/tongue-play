(ns tongue-play.core
  (:require [tongue.core :as tongue])
  (:gen-class))

(def my-dict
  {:en {:animals {:dog "Dog"
                  :cat "Cat"
                  :flying {:bird "Bird"
                           :dinosaur "Dinosaur"}}}
   :tongue/fallback :en})

(def tr (tongue/build-translate my-dict))

(defn -main [& args]
  (prn (tr :en :animals.flying/bird)))
