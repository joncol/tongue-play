(defproject tongue-play "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [tongue "0.2.4"]]
  :main ^:skip-aot tongue-play.core
  :target-path "target/%s"
  :profiles {:dev {:jvm-opts ["-Dclojure.spec.check-asserts=true"]}
             :uberjar {:aot :all
                       :jvm-opts ["-Dclojure.spec.compile-asserts=false"]}})
